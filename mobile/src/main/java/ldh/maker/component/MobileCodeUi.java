package ldh.maker.component;

import javafx.scene.control.TreeItem;
import ldh.database.Table;
import ldh.maker.code.CreateCode;
import ldh.maker.code.MobileCreateCode;
import ldh.maker.db.SettingDb;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;

import java.sql.SQLException;

/**
 * Created by ldh on 2017/4/6.
 */
public class MobileCodeUi extends CodeUi {

    public MobileCodeUi(TreeItem<TreeNode> treeItem, String dbName, String tableName) {
        super(treeItem, dbName, tableName);
    }

    @Override
    protected ColumnUi createColumnPane(TreeItem<TreeNode> treeItem, String dbName, String tableName) {
        return new MobileColumnUi(treeItem, dbName, tableName, this);
    }

    @Override
    protected CreateCode buildCreateCode(Table table) throws SQLException {
        SettingData data = SettingDb.loadData(treeItem.getValue().getParent(), dbName);
        CreateCode createCode = new MobileCreateCode(data, treeItem, dbName, table);
        return createCode;
    }
}
