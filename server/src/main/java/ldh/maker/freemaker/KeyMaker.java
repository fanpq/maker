package ldh.maker.freemaker;

import ldh.bean.util.BeanInfoUtil;
import ldh.bean.util.ValidateUtil;
import ldh.database.PrimaryKey;
import ldh.maker.util.FreeMakerUtil;

public class KeyMaker extends BeanMaker<KeyMaker> {
	
	protected PrimaryKey primaryKey;
	
	public KeyMaker make() {
		data();
		out("key.ftl", data);
		
		return this;
	}
	
	public KeyMaker primaryKey(PrimaryKey primaryKey) {
		this.primaryKey = primaryKey;
		return this;
	}
	
	public KeyMaker imports(Class<?> clazz) {
		imports.add(clazz.getName());
		return this;
	}
	
	public KeyMaker implement(Class<?> clazz) {
		implementList.add(clazz.getName());
		boolean t = BeanInfoUtil.isSerializable(clazz);
		if (t) serializable = true;
		return this;
	}
	
	public KeyMaker className(String className) {
		this.className = className;
		return this;
	}
	
	public KeyMaker pack(String pack) {
		this.pack = pack;
		return this;
	}
	
	public KeyMaker extend(Class<?> extendsClass, String extendsClassName) {
		if (extendsClass != null) {
			this.extendsClassName = extendsClassName;
		}
		if (ValidateUtil.notEmpty(extendsClassName)) {
			extend(extendsClass);
		}
		return this;
	}
	
	public KeyMaker extend(Class<?> extendsClass) {
		imports.add(extendsClass.getName());
		this.extendsClass = extendsClass;
		this.extendsClassName = extendsClass.getName();
		return this;
	}
	
	public String getName() {
		String name = pack + "." + className;
		return name;
	}
	
	public String getSimpleName() {
		return className;
	}
	
	public boolean isSerializable() {
		if (serializable) return true;
		if (extendsClass != null) {
			boolean t = BeanInfoUtil.isSerializable(extendsClass);
			if (t) {
				serializable = true;
			}
		}
		return serializable;
	}
	
	public void data() {
		check();

		data.put("package", pack);
		data.put("className", className);
		data.put("serializable", isSerializable());
		
		data.put("primaryKey", this.primaryKey);
		
//		data.put("config", MakerConfig.getInstance());
		
		if (extendsClassName != null) data.put("extend", extendsClassName);
		if (implementList.size() > 0) data.put("implements", implementList);
		if (imports.size() > 0) data.put("imports", FreeMakerUtil.imports(imports));
	}
	
	protected void check() {
		if (primaryKey == null) {
			throw new NullPointerException("primaryKey must not be null");
		}
		if (className == null || className.trim().equals("")) {
			className = FreeMakerUtil.firstUpper(primaryKey.getKeyName());
		}
		if (fileName == null || fileName.trim().equals("")) {
			fileName = className + ".java";
		}
		super.check();
		if (pack == null && pack.trim().equals("")) {
			throw new NullPointerException("package must not be null");
		}
		
	}
	
	public static void main(String[] args) {
//		String outPath = "E:\\project\\eclipse\\datacenter\\website_statistics\\admin\\src\\main\\base\\ldh\\base\\make\\freemaker";
//		MyMapper other = new MyMapper();
//		other.table("scenic").other("status", Status.class);
//		TableInfo mi = new TableInfo(other);
//		
//		Table table = mi.getTable("scenic");
		
//		BeanMaker bm = new BeanMaker()
//		 	.pack("ldh.base.make.freemaker")
//		 	.extend(CommonEntity.class, "CommonEntity<Integer>")
//		 	.outPath(outPath)
//		 	.make();
//		
//		
//		new BeanWhereMaker()
//		 	.pack("ldh.base.make.freemaker")
//		 	.outPath(outPath)
//		 	.className(FreeMakerUtil.beanName(table.getName()) + "Where")
//		 	.extend(FreeMakerUtil.beanName(table.getName()))
//		 	.imports(bm.getName())
//		 	.implement(Pageable.class)
//		 	.serializable(bm.isSerializable())
//		 	.make();
//		
	}
}
