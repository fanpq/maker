package ${package};

<#if imports?exists>
	<#list imports as import>
import ${import};
	</#list>
</#if>

/**
* @author: ${Author}
* @date: ${DATE}
*/
public interface ${className} <#if implements?exists>extends <#list implements as implement>${implement}<#if implement_has_next>,</#if></#list></#if>${r'{'}

	<#--------------insert-------------------------------------------------------------------------------------------------->
	<#if util.isCreate(table, "insert") >
	Integer insert(${bean} ${util.javaName(bean)});
	</#if>

	<#--------------insert-------------------------------------------------------------------------------------------------->
	<#if util.isCreate(table, "insertSelective") >
	Integer insertSelective(${bean} ${util.javaName(bean)});
	</#if>

	<#--------------update------------------------------------------------------------------------------------------------->
	<#list table.indexies as index>
	<#if util.isCreate(table, "updateBy${util.uniqueName(index)}") >
	Integer updateBy${util.uniqueName(index)}(${bean} ${util.javaName(bean)});
	
	</#if>
	<#--------------updateNotnull------------------------------------------------------------------------------------------>
	<#if util.isCreate(table, "updateNotNullBy${util.uniqueName(index)}") >
	Integer updateNotNullBy${util.uniqueName(index)}(${bean} ${util.javaName(bean)});
	
	</#if>
	<#--------------delete------------------------------------------------------------------------------------------------->
	<#if util.isCreate(table, "deleteBy${util.uniqueName(index)}") >
	Integer deleteBy${util.uniqueName(index)}(${util.uniqueParamDao(table, index)});

	</#if>
	</#list>
	<#-----------------------------------getUnique------------------------------------------------------------------------->
	<#list table.indexies as index>	
	<#if util.isUnique(index, table)>
	<#if util.isCreate(table, "getBy" + util.uniqueName(index)) >
	${bean} getBy${util.uniqueName(index)}(${util.uniqueParamDao(table, index)});

	</#if>
	<#--------------getWith------------------------------------------------------------------------------------------------>
		<#list table.foreignKeys as foreignKey>
		<#if util.isCreate(table, "getWith${util.firstUpper(foreignKey.column.property)}By" + util.uniqueName(index)) >
	${bean} getWith${util.firstUpper(foreignKey.column.property)}By${util.uniqueName(index)}(${util.uniqueParamDao(table, index)});

			</#if>
		</#list>
	<#--------------getWithAssociatesById---------------------------------------------------------------------------------->
		<#if table.foreignKeys?size gt 1>
			<#if util.isCreate(table, "getWithAssociatesBy" + util.uniqueName(index)) >
	${bean} getWithAssociatesBy${util.uniqueName(index)}(${util.uniqueParamDao(table, index)});

			</#if>
		</#if>
	<#--------------getJoin------------------------------------------------------------------------------------------------>
		<#list table.foreignKeys as foreignKey>
			<#if util.isCreate(table, "getJoin${util.firstUpper(foreignKey.column.property)}By"+ util.uniqueName(index)) >
	${bean} getJoin${util.firstUpper(foreignKey.column.property)}By${util.uniqueName(index)}(${util.uniqueParamDao(table, index)});

			</#if>
		</#list>
	<#--------------getJoinAssociatesById---------------------------------------------------------------------------------->
		<#if table.foreignKeys?size gt 1>
			<#if util.isCreate(table, "getJoinAssociatesBy"+ util.uniqueName(index)) >
	${bean} getJoinAssociatesBy${util.uniqueName(index)}(${util.uniqueParamDao(table, index)});

			</#if>
		</#if>
	<#--------------getManyJoin-------------------------------------------------------------------------------------------->
		<#if table.many?? && table.many?size gt 0 >
			<#list table.many as manyOne>
				<#if util.isCreate(table, "getJoin${util.firstUpper(manyOne.table.javaName)}By"+ util.uniqueName(index)) >
	${bean} getJoin${util.firstUpper(manyOne.table.javaName)}By${util.uniqueName(index)}(${util.uniqueParamDao(table, index)});

				</#if>
			</#list>
		</#if>
	<#--------------getManyJoinAllById------------------------------------------------------------------------------------->
		<#if table.many?size gt 1>
			<#if util.isCreate(table, "getJoinAllBy"+ util.uniqueName(index)) >
	${bean} getJoinAllBy${util.uniqueName(index)}(${util.uniqueParamDao(table, index)});

			</#if>
		</#if>
	<#--------------getManyToMany------------------------------------------------------------------------------------->
		<#if table.manyToManys?? && table.manyToManys?size gt 0 >
			<#list table.manyToManys as mm>
				<#if util.isCreate(mm.primaryTable, "getMany" + util.firstUpper(mm.secondTable.javaName) + "sBy"+ util.uniqueName(index)) >
	${bean} getMany${util.firstUpper(mm.secondTable.javaName)}sBy${util.uniqueName(index)}(${util.uniqueParamDao(table, index)});

				</#if>
			</#list>
		</#if>
	</#if>
	</#list>
	<#--------------find--------------------------------------------------------------------------------------------------->
	<#if util.isCreate(table, "find") >
	List<${bean}> findBy${beanWhere}(${beanWhere} ${util.javaName(beanWhere)});
	
	</#if>
	<#--------------findTotal---------------------------------------------------------------------------------------------->
	<#if util.isCreate(table, "find") >
	Long findTotalBy${beanWhere}(${beanWhere} ${util.javaName(beanWhere)});
	
	</#if>
	<#--------------findByJoin--------------------------------------------------------------------------------------------->
	<#if table.foreignKeys?? && table.foreignKeys?size gt 0 >
	<#if util.isCreate(table, "findJoinBy") >
	List<${bean}> findJoinBy${beanWhere}(${beanWhere} ${util.javaName(beanWhere)});
	
	</#if>
	<#--------------findTotalByJoin---------------------------------------------------------------------------------------->
	<#if util.isCreate(table, "findJoinBy") >
	Long findJoinTotalBy${beanWhere}(${beanWhere} ${util.javaName(beanWhere)});
	</#if>
	</#if>
	
${r'}'}
