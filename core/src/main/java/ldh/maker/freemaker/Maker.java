package ldh.maker.freemaker;

public abstract class Maker<T> {

	protected String outPath;
	protected String fileName;
	
	@SuppressWarnings("unchecked")
	public T outPath(String outPath) {
		this.outPath = outPath;
		return ((T) this);
	}
	
	@SuppressWarnings("unchecked")
	public T fileName(String fileName) {
		this.fileName = fileName;
		return ((T) this);
	}
	
	public abstract T make();
	
	protected void check() {
//		if (outPath == null || outPath.trim().equals("")) {
//			throw new RuntimeException("outPath must not be null!!!!!!!!!!");
//		}
		if (fileName == null || fileName.trim().equals("")) {
			throw new RuntimeException("fileName must not be null!!!!!!111");
		} 
	}
}
