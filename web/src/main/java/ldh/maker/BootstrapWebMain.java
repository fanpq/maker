package ldh.maker;

import ldh.maker.component.BootstrapContentUiFactory;
import ldh.maker.util.UiUtil;

public class BootstrapWebMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new BootstrapContentUiFactory());
    }

    @Override
    protected String getTitle() {
        return "Bootstrap前后端--代码自动生成工具";
    }

    public static void main(String[] args) throws Exception {
        startDb(args);
        launch(args);
    }
}

