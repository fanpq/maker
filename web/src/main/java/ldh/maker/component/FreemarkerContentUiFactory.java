package ldh.maker.component;

public class FreemarkerContentUiFactory extends ContentUiFactory {

    public ContentUi create() {
        return new FreemarkerContentUi();
    }
}
